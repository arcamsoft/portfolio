type ContactLink = {
  name: string;
  asset: string;
  url: string;
};

export const CONTACT_LINKS: ContactLink[] = [
  {
    name: "Linkedin",
    asset: "/linkedin.svg",
    url: "https://www.linkedin.com/in/alex-camara/",
  },
];
