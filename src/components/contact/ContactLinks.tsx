import { CONTACT_LINKS } from "./config";
import linkedinIcon from "../../assets/icons/linkedin.svg";

interface ContactLinksProps {}

const ContactLinks: React.FC<ContactLinksProps> = () => {
  return (
    <div className="hidden md:flex fixed z-30 h-screen flex-col justify-center items-center px-6">
      {CONTACT_LINKS.map((contactLink) => (
        <span key={`contact-link-${contactLink.name}`} className="relative flex h-7 w-7">
          <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-teal-700 opacity-75" />
          <a
            href={contactLink.url}
            target="_blank"
            className="relative inline-flex justify-center rounded-full h-7 w-7 bg-teal-800"
          >
            <img
              src={linkedinIcon.src}
              className="w-4 blue-icon hover:w-5 transition-all"
            />
          </a>
        </span>
      ))}
    </div>
  );
};

export default ContactLinks;
