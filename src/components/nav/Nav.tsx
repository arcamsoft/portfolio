import { useEffect, useState } from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import Drawer from "./Drawer";
import TopBar from "./TopBar";

interface NavProps {}

const Nav: React.FC<NavProps> = () => {
  const [currentScrollY, setCurrentScrollY] = useState<number>(0);
  const [isNavOpen, setIsNavOpen] = useState<boolean>(true);
  const [isMobileNavOpen, setIsMobileNavOpen] = useState<boolean>(false);

  useEffect(() => {
    AOS.init();
  }, []);

  const handleScroll = (newScrollY: number) => {
    const showNavbar = Boolean(currentScrollY > newScrollY);
    setIsNavOpen(showNavbar);
    setCurrentScrollY(newScrollY);
  };

  useEffect(() => {
    window.addEventListener("scroll", () => handleScroll(window.scrollY), {
      passive: true,
    });
    return () =>
      window.removeEventListener("scroll", () => handleScroll(window.scrollY));
  }, [window.scrollY]);

  const handleGoToSection = (section: string) => {
    window.location.assign(section);
    setIsMobileNavOpen(false);
  };

  const handleToggleMobileMenu = () => {
    setIsMobileNavOpen(!isMobileNavOpen);
  };

  return (
    <nav
      className={`fixed left-0 right-0 md:opacity-80 items-center justify-between bg-background transition-all ease-in-out z-20 
      ${isNavOpen ? "-mt-0" : "-mt-20"}
      `}
    >
      <Drawer
        isMobileNavOpen={isMobileNavOpen}
        onToggleMobileMenu={handleToggleMobileMenu}
        onGoToSection={handleGoToSection}
      />

      <TopBar onGoToSection={handleGoToSection} />
    </nav>
  );
};

export default Nav;
