import Link from "../UI/Link";
import NavItem from "../UI/NavItem";

interface DrawerProps {
  isMobileNavOpen: boolean;
  onToggleMobileMenu: () => void;
  onGoToSection: (section: string) => void;
}

const Drawer: React.FC<DrawerProps> = ({
  isMobileNavOpen,
  onToggleMobileMenu,
  onGoToSection,
}) => {
  return (
    <div className="md:hidden flex justify-end w-full relative py-4">
      <button
        className={`hamburger--elastic flex h-12 items-center self-center z-50 pr-6 ${
          isMobileNavOpen ? "is-active fixed right-0 top-4" : ""
        }`}
        onClick={() => onToggleMobileMenu()}
      >
        <span className="hamburger-box">
          <span className="hamburger-inner" />
        </span>
      </button>

      <aside
        className={`transform top-0 w-3/4 pt-24 bg-light-primary fixed h-full ease-in-out transition-all duration-300 z-40 ${
          isMobileNavOpen ? "-translate-x-3/2" : "translate-x-full"
        }`}
      >
        <ul className="flex items-center flex-col mx-auto">
          <li
            className="flex items-center p-4 md:hidden"
            onClick={() => onGoToSection("#about-me")}
          >
            <NavItem text="About me" />
          </li>
          <li
            className="flex items-center p-4 md:hidden"
            onClick={() => onGoToSection("#experience")}
          >
            <NavItem text="Experience" />
          </li>
          <li
            className="flex items-center p-4 md:hidden"
            onClick={() => onGoToSection("#projects")}
          >
            <NavItem text="Projects" />
          </li>
          <li
            className="flex items-center p-4 md:hidden"
            onClick={() => onGoToSection("#contact")}
          >
            <NavItem text="Contact" />
          </li>
          <li
            className="flex items-center p-4 w-3/4 md:hidden"
            onClick={() => onGoToSection("#contact")}
          >
            <Link
            className="w-full flex justify-center"
              text="Resume"
              href="/assets/resume/resume_alejandro_camara_software_engineer.pdf"
            />
          </li>
        </ul>
      </aside>
    </div>
  );
};

export default Drawer;
