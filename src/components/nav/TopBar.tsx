import Link from "../UI/Link";
import NavItem from "../UI/NavItem";

interface TopBarProps {
  onGoToSection: (section: string) => void;
}

const TopBar: React.FC<TopBarProps> = ({ onGoToSection }) => {
  return (
    <div
      className={`hidden md:flex items-center justify-between w-full px-8 py-4`}
    >
      <div
        className="hidden lg:flex my-auto self-start mr-auto "
        data-aos="fade-right"
        data-aos-duration="2000"
        data-aos-delay="200"
        data-aos-once="true"
      >
        <img
          src="/assets/logo.svg"
          className="h-10 cursor-pointer"
        />
      </div>

      <ul className="flex gap-x-4">
        <li
          data-aos="fade-down"
          data-aos-duration="1000"
          data-aos-delay="100"
          onClick={() => onGoToSection("#about-me")}
        >
          <NavItem text="About me" />
        </li>
        <li
          data-aos="fade-down"
          data-aos-duration="1000"
          data-aos-delay="300"
          onClick={() => onGoToSection("#experience")}
        >
          <NavItem text="Experience" />
        </li>
        <li
          data-aos="fade-down"
          data-aos-duration="1000"
          data-aos-delay="500"
          onClick={() => onGoToSection("#projects")}
        >
          <NavItem text="Projects" />
        </li>
        <li
          data-aos="fade-down"
          data-aos-duration="1000"
          data-aos-delay="700"
          onClick={() => onGoToSection("#contact")}
        >
          <NavItem text="Contact" />
        </li>
        <li
          data-aos="fade-left"
          data-aos-duration="1000"
          data-aos-delay="900"
          className="flex"
        >
          <Link
            text="Resume"
            href="/assets/resume/resume_alejandro_camara_software_engineer.pdf"
          />
        </li>
      </ul>
    </div>
  );
};

export default TopBar;
