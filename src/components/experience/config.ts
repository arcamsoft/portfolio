type ProjectTask = {
  description: string;
  technologies: string[];
};

type JobProject = {
  name?: string;
  tasks: ProjectTask[];
};

export type Job = {
  company: {
    name: string;
    alias: string;
    url: string;
  };
  jobTitle: string;
  startDate: string;
  endDate: string;
  projects: JobProject[];
};

export const JOBS_CONFIG: Job[] = [
  {
    company: {
      name: "Konfío",
      alias: "Konfío",
      url: "https://konfio.mx/",
    },
    jobTitle: "Software Engineer",
    startDate: "November 2021",
    endDate: "Present",
    projects: [
      {
        name: "",
        tasks: [
          {
            description:
              "Automated the way in which a user would provide the shareholding chart of its company by:",
            technologies: [],
          },
          {
            description:
              "Reviewed and maintained the web and api git repositories, providing useful feedback, establishing general coding conventions and standards and proposing new tools, techniques, patterns and refactors to improve the code base",
            technologies: [],
          },
          {
            description:
              "Looked for opportunity areas in existing tasks and processes in order to present and discuss them with our team",
            technologies: [],
          },
          {
            description:
              "Performed constant monitoring and correcting activities in all initiatives I was part of",
            technologies: [],
          },
          {
            description:
              "Collaborated in documenting in detail loans and payments processes and keeping updates of every major change",
            technologies: [],
          },
          {
            description:
              "Implemented an automated process to notify a user the result of his loan request. Given the asynchronicity of the process, AWS ApiGateway, SNS and Lambda resources were used",
            technologies: [],
          },
          {
            description:
              "Migrated web monorepo configuration tool from lerna no NX",
            technologies: [],
          },
          // {
          //   description: "",
          //   technologies: [],
          // },
        ],
      },
    ],
  },
  {
    company: {
      name: "Koala Workshop",
      alias: "Koala",
      url: "https://pacoelchato.com/",
    },
    jobTitle: "Software Engineer",
    startDate: "March 2021",
    endDate: "November 2021",
    projects: [
      {
        name: "Paco el Chato - Learning lab",
        tasks: [
          {
            description:
              "Implemented the frontend side for a student’s learning site new section called ‘learning lab’, aimed to strengthen the user’s grasp in previous concepts learned at the main site.",
            technologies: ["Laravel", "Vue"],
          },
        ],
      },
      {
        name: "Beelinguist",
        tasks: [
          {
            description:
              "Analyzed an existing web platform in order to accomodate new requirements.",
            technologies: ["Laravel", "Vue"],
          },
          {
            description:
              "Reimplemented the platform’s frontend side in order to support API calls instead of laravel web routes.",
            technologies: ["Laravel", "Vue"],
          },
        ],
      },
    ],
  },
  {
    company: {
      name: "iDisc Information Technologies",
      alias: "iDisc",
      url: "https://www.idisc.com/",
    },
    jobTitle: "Software Engineer",
    startDate: "November 2020",
    endDate: "November 2021",
    projects: [
      {
        name: "Xpress 2.0",
        tasks: [
          {
            description:
              "Interpreted and translated internal user requirements in order to propose an automated tool to assist in the daily tasks performed by the company’s linguists.",
            technologies: [],
          },
          {
            description:
              "Automated the company’s internal task translation process by the creation of a website platform that could implement the previously identified requirements.",
            technologies: ["Laravel", "React"],
          },
        ],
      },
      {
        name: "Sangüesa CRM",
        tasks: [
          {
            description:
              "Migrated and evolved a client’s CRM in order to meet new requirements. Developed the product in several iterations in order to involve customer’s feedback and maximize the new product’s acceptation by the client.",
            technologies: ["Laravel"],
          },
        ],
      },
    ],
  },
  {
    company: {
      name: "Observatorio de Educación Médica y Derechos Humanos",
      alias: "OBEME",
      url: "https://www.uv.mx/isp/",
    },
    jobTitle: "Software Engineer Intern",
    startDate: "November 2019",
    endDate: "July 2020",
    projects: [
      {
        name: "ObemeVeme Android / Web",
        tasks: [
          {
            description:
              "Implemented requirements engineering activities to develop an application intended to serve as a tool to collect data from medical centers by health sciences students, said data would allow competent authorities to analyze working conditions.",
            technologies: [],
          },
          {
            description:
              "Designed and constructed the application. Construction was split in three modules: backend, using SpringBoot, Kotlin and PostgreSQL; web frontend, implemented with the VueJS framework; and the Android app.",
            technologies: [],
          },
        ],
      },
    ],
  },
  {
    company: {
      name: "Instituto de Investigaciones Biológicas de la UV",
      alias: "IIBUV",
      url: "https://www.uv.mx/iib/",
    },
    jobTitle: "Software Engineer",
    startDate: "February 2019",
    endDate: "August 2020",
    projects: [
      {
        name: "PROGEFI",
        tasks: [
          {
            description:
              "Implemented requirements engineering activities in order to develop an application intended oto create and manage photocollect data cards for the institute.",
            technologies: [],
          },
          {
            description:
              "Designed and constructed the application using the following stack: ElectronJS framework for multiplatform support; VueJS for user interface",
            technologies: ["Node", "Vue", "SQLite"],
          },
          {
            description:
              "Verification and validation with the intended users in the institute.",
            technologies: [],
          },
        ],
      },
    ],
  },
  {
    company: {
      name: "Instituto de Salud Pública de la UV",
      alias: "ISP",
      url: "https://www.uv.mx/isp/",
    },
    jobTitle: "Software Engineer Intern",
    startDate: "February 2019",
    endDate: "September 2019",
    projects: [
      {
        name: "",
        tasks: [
          {
            description:
              "Elicited requirements for an application to assist patients released from drug dependency rehabilitation centers to prevent a future relapse.",
            technologies: [],
          },
          {
            description:
              "Documented those requirements in a software requirements specification document to help estimate the app development cost for the institute.",
            technologies: [],
          },
          {
            description:
              "Designed the application high-fidelity prototype to validate the requirements with potential users in rehabcenters in Xalapa, Veracruz.",
            technologies: [],
          },
        ],
      },
    ],
  },
  {
    company: {
      name: "Laboratorio Nacional de Informática Avanzada",
      alias: "LANIA",
      url: "https://www.lania.mx/",
    },
    jobTitle: "Software Engineer Intern",
    startDate: "September 2018",
    endDate: "December 2018",
    projects: [
      {
        name: "",
        tasks: [
          {
            description:
              "Ellaborated an evaluation report regarding available testing tools for VueJS and ExpressJS frameworks",
            technologies: [],
          },
          {
            description:
              "Automated the institution workers paid leave permits process to reduce paper and bureaucratic work.",
            technologies: [],
          },
        ],
      },
    ],
  },
];
