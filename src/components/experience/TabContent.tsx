import type { Job } from "./config";
import terminalIcon from "../../assets/icons/terminal.svg";

interface TabContentProps {
  selectedTab: Job;
}

const TabContent: React.FC<TabContentProps> = ({ selectedTab }) => {
  return (
    <div className="text-md">
      <h4 className="font-medium">
        {selectedTab.jobTitle} at{" "}
        <a className="link-text" href={selectedTab.company.url} target="blank">
          {selectedTab.company.name}
        </a>
      </h4>
      <p className="font-light">{`${selectedTab.startDate} - ${selectedTab.endDate}`}</p>
      <ul className="mt-4">
        {selectedTab.projects.map((project, index) => (
          <li key={`company-project-${index}`} className="mb-4">
            <p className="my-1">{project.name}</p>
            <ul className="flex flex-col gap-y-3">
              {project.tasks.map((task, taskIndex) => (
                <li key={`task-${taskIndex}`} className="flex">
                  <img
                    className="blue-icon h-4 mt-2 mr-1"
                    src={terminalIcon.src}
                  />
                  <p>
                    {task.description}{" "}
                    {Boolean(task.technologies.length) && (
                      <span className="font-extralight">{`(${task.technologies.join(
                        ", "
                      )})`}</span>
                    )}
                  </p>
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TabContent;
