import { useState } from "react";
import CompanyTabs from "./CompanyTabs";
import { JOBS_CONFIG, type Job } from "./config";
import TabContent from "./TabContent";

interface TimelineProps {}

const Timeline: React.FC<TimelineProps> = () => {
  const companies = JOBS_CONFIG.map((job) => job.company.alias);
  const [selectedTab, setSelectedTab] = useState<Job>(JOBS_CONFIG[0]);

  const handleTabSelected = (companyAlias: string) => {
    const selectedJob = JOBS_CONFIG.find(
      (job) => job.company.alias === companyAlias
    );
    if (!selectedJob) return null;
    setSelectedTab(selectedJob);
  };

  return (
    <div className="md:flex md:gap-x-10 h-[40rem] w-full">
      <div
        className="h-20 md:h-auto"
        data-aos="fade-right"
        data-aos-duration="2000"
        data-aos-once="true"
      >
        <CompanyTabs
          companies={companies}
          onTabSelected={handleTabSelected}
          selectedTab={selectedTab}
        />
      </div>
      <div data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
        <TabContent selectedTab={selectedTab} />
      </div>
    </div>
  );
};

export default Timeline;
