import type { Job } from "./config";

interface CompanyTabsProps {
  companies: string[];
  selectedTab: Job;
  onTabSelected: (companyAlias: string) => void;
}

const CompanyTabs: React.FC<CompanyTabsProps> = ({
  companies,
  selectedTab,
  onTabSelected,
}) => {
  return (
    <ul className="flex absolute md:relative left-4 right-4 md:flex-col overflow-x-scroll md:overflow-visible md:w-1/4">
      {companies.map((company) => (
        <li
          key={`company-${company}`}
          className={`tab-button ${
            selectedTab.company.alias === company ? "tab-button-selected" : ""
          }`}
          onClick={() => onTabSelected(company)}
        >
          {company}
        </li>
      ))}
    </ul>
  );
};

export default CompanyTabs;
