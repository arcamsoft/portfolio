interface LinkProps {
  text: string;
  href: string;
  className?: string;
}

const Link: React.FC<LinkProps> = ({ text, className, href }) => {
  return (
    <a href={href} target="_blank" className={`regular-button ${className}`} >
      {text}
    </a>
  );
};

export default Link;
