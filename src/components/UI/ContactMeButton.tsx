interface ContactMeButtonProps {
  text: string;
  className?: string;
}

const ContactMeButton: React.FC<ContactMeButtonProps> = ({
  text,
  className,
}) => {
  const handleGoToSection = () => {
    window.location.assign("#contact");
  };

  return (
    <button
      className={`regular-button ${className}`}
      onClick={() => handleGoToSection()}
    >
      {text}
    </button>
  );
};

export default ContactMeButton;
