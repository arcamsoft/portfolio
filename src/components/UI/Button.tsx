interface ButtonProps {
  text: string;
  className?: string;
  onClick?: () => void;
}

const Button: React.FC<ButtonProps> = ({ text, className, onClick }) => {
  return (
    <button className={`regular-button ${className}`} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;
