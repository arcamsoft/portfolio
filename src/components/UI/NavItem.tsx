interface NavItemProps {
  text: string;
}

const NavItem: React.FC<NavItemProps> = ({ text }) => {
  return (
    <button className="md:py-2 px-3 flex items-center gap-x-1 hover:text-accent">
      <div className="hidden md:block h-1 w-1 bg-accent rounded-full" />
      <p>{text}</p>
    </button>
  );
};

export default NavItem;
