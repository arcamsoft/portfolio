import { type PinnedProjectVariant, type Project } from "../config";
import githubIcon from "../../../assets/icons/github.svg";
import linkIcon from "../../../assets/icons/link.svg";

interface ProjectDescriptionProps {
  project: Project;
  variant: PinnedProjectVariant;
}

const DescriptionBox: React.FC<{
  description: string;
  isVariantLeft: boolean;
}> = ({ description, isVariantLeft }) => {
  const descriptionMargin = isVariantLeft ? "-mr-32" : "-ml-32";
  return (
    <div
      className={`text-light-dark shadow-2xl bg-light-primary p-6 z-10 text-left text-sm ${descriptionMargin}`}
    >
      {description}
    </div>
  );
};

const TechnologiesList: React.FC<{
  technologies: string[];
  isVariantLeft: boolean;
}> = ({ technologies, isVariantLeft }) => {
  return (
    <div
      className={`flex gap-x-3 text-md text-accent ${
        isVariantLeft ? "" : "self-end"
      }`}
    >
      {technologies.map((technology) => (
        <p>{technology}</p>
      ))}
    </div>
  );
};

const LinkButtons: React.FC<{
  repositoryLink: string;
  projectLink?: string;
  isVariantLeft: boolean;
}> = ({ repositoryLink, projectLink, isVariantLeft }) => {
  return (
    <div className={`flex gap-x-4 ${isVariantLeft ? "" : "self-end"}`}>
      <a href={repositoryLink} target="_blank">
        <img src={githubIcon.src} className="white-icon w-5" />
      </a>
      {projectLink && (
        <a href={repositoryLink} target="_blank">
          <img src={linkIcon.src} className="white-icon w-5" />
        </a>
      )}
    </div>
  );
};

const ProjectDescription: React.FC<ProjectDescriptionProps> = ({
  project,
  variant,
}) => {
  const isVariantLeft = variant === "left";
  const textAlignment = isVariantLeft ? "text-left" : "text-right";
  return (
    <div
      className={`flex flex-col h-full justify-center gap-y-3 ${textAlignment}`}
    >
      <div>
        <p className="text-accent">Pinned project</p>
        <p className="text-medium-white font-semibold text-lg">
          {project.name}
        </p>
      </div>
      <DescriptionBox
        description={project.description}
        isVariantLeft={isVariantLeft}
      />

      <TechnologiesList
        technologies={project.technologies}
        isVariantLeft={isVariantLeft}
      />

      <LinkButtons
        repositoryLink={project.link.repository}
        projectLink={project.link.project}
        isVariantLeft={isVariantLeft}
      />
    </div>
  );
};

export default ProjectDescription;
