import type { PinnedProject, PinnedProjectVariant } from "../config";
import ProjectDescription from "./ProjectDescription";
import ProjectImages from "./ProjectImages";

interface PinnedProjectSectionProps {
  project: PinnedProject;
  variant: PinnedProjectVariant;
}

const PinnedProjectSection: React.FC<PinnedProjectSectionProps> = ({
  project,
  variant,
}) => {
  if (variant === "right") {
    return (
      <div className="flex">
        <div
          className="w-1/2 md:w-4/6"
          data-aos="fade-right"
          data-aos-duration="2000"
          data-aos-once="true"
        >
          <ProjectImages images={project.images} />
        </div>
        <div
          className="w-1/2 md:w-2/6"
          data-aos="fade-left"
          data-aos-duration="2000"
          data-aos-once="true"
        >
          <ProjectDescription project={project} variant={variant} />
        </div>
      </div>
    );
  }

  return (
    <div className="flex h-full">
      <div
        className="w-1/2 md:w-2/6 z-10"
        data-aos="fade-right"
        data-aos-duration="1000"
        data-aos-once="true"
      >
        <ProjectDescription project={project} variant={variant} />
      </div>
      <div
        className="w-1/2 md:w-4/6"
        data-aos="fade-left"
        data-aos-duration="2000"
        data-aos-once="true"
      >
        <ProjectImages images={project.images} />
      </div>
    </div>
  );
};

export default PinnedProjectSection;
