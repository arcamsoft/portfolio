interface ProjectImagesProps {
  images: string[];
}

const ProjectImages: React.FC<ProjectImagesProps> = ({ images }) => {
  return (
    <div className="flex justify-between gap-x-4 md:gap-x-14">
      {images.map((image) => (
        <div key={`project-image-${image}`} className="w-full">
          <img
            className={`w-full rounded-md w-1/${images.length} lg:accent-filter hover:no-filter`}
            src={image}
          />
        </div>
      ))}
    </div>
  );
};

export default ProjectImages;
