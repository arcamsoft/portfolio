import { type BaseProject } from "../config";
import folderIcon from "../../../assets/icons/folder.svg";
import githubIcon from "../../../assets/icons/github.svg";

interface BaseProjectSectionProps {
  project: BaseProject;
}

const BaseProjectSection: React.FC<BaseProjectSectionProps> = ({ project }) => {
  return (
    <div className="bg-light-primary p-4 md:mx-3 rounded-lg flex flex-col h-full project-box">
      <div className="flex flex-col gap-y-6">
        <div className="flex justify-between">
          <img src={folderIcon.src} className="blue-icon w-10" />
          <a href={project.link.repository} target="_blank">
            <img src={githubIcon.src} className="white-icon w-5" />
          </a>
        </div>
        <h3 className="text-white  font-medium">{project.name}</h3>
      </div>
      <p className="text-sm my-8">{project.description}</p>
      <div className="text-xs flex gap-x-2 mt-auto">
        {project.technologies.map((technology) => (
          <p>{technology}</p>
        ))}
      </div>
    </div>
  );
};

export default BaseProjectSection;
