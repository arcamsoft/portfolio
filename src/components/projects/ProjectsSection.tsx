import {
  PINNED_PROJECTS_CONFIG,
  type BaseProject,
  type PinnedProject,
  OTHER_PROJECTS_CONFIG,
} from "./config";
import PinnedProjectSection from "./pinnedProject/PinnedProjectSection";
import BaseProjectSection from "./baseProject/BaseProjectSection";

const ProjectsSection: React.FC = () => {
  return (
    <div className="flex flex-col gap-y-28 md:gap-y-44">
      {PINNED_PROJECTS_CONFIG.map((project, index) => (
        <PinnedProjectSection
          key={`pinned-project-${index}`}
          project={project as PinnedProject}
          variant={index % 2 === 0 ? "right" : "left"}
        />
      ))}

      <div className="flex flex-col items-center gap-y-10 my-10">
        <h2
          className="text-medium-white font-semibold text-xl"
          data-aos="fade-left"
          data-aos-duration="2000"
          data-aos-once="true"
        >
          Other projects
        </h2>

        <div className="flex flex-col md:flex-row flex-wrap w-full justify-center">
          {OTHER_PROJECTS_CONFIG.map((project, index) => (
            <div
              key={`other-project-${index}`}
              data-aos={index % 2 === 0 ? "fade-right" : "fade-left"}
              data-aos-duration="2000"
              data-aos-once="true"
              className="w-full md:w-1/3 md:min-h-80 my-3"
            >
              <BaseProjectSection project={project as BaseProject} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProjectsSection;
