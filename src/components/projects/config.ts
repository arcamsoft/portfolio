import progefiImage from "../../assets/projects/progefi/progefi.png";
import obemeImage1 from "../../assets/projects/obeme/obeme1.png";
import obemeImage2 from "../../assets/projects/obeme/obeme2.png";
import obemeImage3 from "../../assets/projects/obeme/obeme3.png";

export type BaseProject = {
  isPinned: boolean;
  name: string;
  description: string;
  technologies: string[];
  link: {
    repository: string;
    project?: string;
  };
};

export type PinnedProject = BaseProject & {
  isPinned: true;
  images: string[];
};

export type Project = PinnedProject | BaseProject;

export type PinnedProjectVariant = "right" | "left";

export const PINNED_PROJECTS_CONFIG: Project[] = [
  {
    isPinned: true,
    name: "PROGEFI",
    description:
      "Desktop application aimed to provide full support to biologist and technicians in their labor to generate and manage photocollect data-cards. Currently in production.",
    technologies: ["Electron", "Vue", "Node", "SQLite"],
    images: [progefiImage.src],
    link: {
      repository: "https://github.com/Alex-Camara/PROGEFI",
      project: "https://alex-camara.github.io/PROGEFI/",
    },
  },
  {
    isPinned: true,
    name: "VemeObeme App",
    description:
      "Android app aimed to collect data from medical students during their residence in hospitals. Currently in production.",
    technologies: ["Android", "Kotlin", "Room", "SpringBoot"],
    images: [obemeImage1.src, obemeImage2.src, obemeImage3.src],
    link: {
      repository: "https://github.com/TequilaCoders/OBEME-Android",
    },
  },
];

export const OTHER_PROJECTS_CONFIG: Project[] = [
  {
    isPinned: false,
    name: "Tequila IDE",
    description:
      "Real time collaborative Integrated Development Environment Application. It allows users to create and contribute in real time, manipulating Java, C++ and Python files.",
    technologies: ["Java", "Node", "Socket.io", "JavaFX"],
    link: {
      repository: "https://github.com/TequilaCoders/TequilaIDE",
    },
  },
  {
    isPinned: false,
    name: "Roomba Music",
    description:
      "Distributed desktop music player designed to share, store, and organize music",
    technologies: ["Java", "Python", "MySQL", "Ubuntu Server"],
    link: {
      repository: "https://github.com/Alex-Camara/AppMusica",
    },
  },
  {
    isPinned: false,
    name: "ObemeVeme Backend",
    description:
      "Backend infrastructure providing the services to the android and web clients.",
    technologies: ["Kotlin", "SpringBoot", "PostgreSQL", "GCP"],
    link: {
      repository: "https://github.com/TequilaCoders/OBEME-Backend",
    },
  },
  {
    isPinned: false,
    name: "Cinco en Línea",
    description: "Cinco en Línea game with artificial intelligence.",
    technologies: ["Java", "Socket.io", "IA"],
    link: {
      repository: "https://github.com/AlanCrux/5EnLinea",
    },
  },
  {
    isPinned: false,
    name: "ResuelveFC",
    description:
      "Simple Ruby CLI program to calculate a player total payment based on a couple of factors.",
    technologies: ["Ruby"],
    link: {
      repository: "https://github.com/Alex-Camara/ResuelveFC",
    },
  },
];
