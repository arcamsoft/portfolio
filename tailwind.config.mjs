/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      colors: {
        accent: "#64ffda",
        background: "#0a192f",
        "light-dark": "#a8b2d1",
        "light-primary": "#172a45",
        "dark-white": "#A8B2D1",
        "dark-gray": "#8892b0",
        "medium-white": "#e6f1ff"
      },
      fontFamily: {
        sans: ["Lato", "sans-serif"],
        // sans: ["Poppins", "sans-serif"],
      },
    },
  },
  plugins: [],
};
